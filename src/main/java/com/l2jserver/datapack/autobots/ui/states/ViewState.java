package com.l2jserver.datapack.autobots.ui.states;

public interface ViewState {
    boolean isActive();
    void setIsActive(boolean isActive);
    void reset();

}
