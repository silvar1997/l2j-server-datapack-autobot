package com.l2jserver.datapack.autobots.behaviors.classes.pre;


import com.l2jserver.datapack.autobots.Autobot;
import com.l2jserver.datapack.autobots.behaviors.CombatBehavior;
import com.l2jserver.datapack.autobots.behaviors.attributes.SecretManaRegen;
import com.l2jserver.datapack.autobots.behaviors.preferences.CombatPreferences;
import com.l2jserver.datapack.autobots.behaviors.preferences.SkillPreferences;
import com.l2jserver.datapack.autobots.skills.BotSkill;
import com.l2jserver.gameserver.enums.ShotType;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;

import java.util.List;

import static com.l2jserver.datapack.autobots.utils.AutobotSkills.shieldStun;
import static com.l2jserver.datapack.autobots.utils.AutobotSkills.ultimateDefense;

public class KnightBehavior extends CombatBehavior implements SecretManaRegen {

    public KnightBehavior(Autobot player, CombatPreferences combatPreferences) {
        super(player, combatPreferences);
        this.skillPreferences = new SkillPreferences(false);
    }

    @Override
    public ShotType getShotType() {
        return ShotType.SOULSHOTS;
    }

    @Override
    protected List<BotSkill> getOffensiveSkills() {
        return List.of(
                new BotSkill(shieldStun, (player, skill, target) -> target instanceof L2PcInstance && !target.isStunned()));
    }

    @Override
    protected List<BotSkill> getSelfSupportSkills() {
        return List.of(new BotSkill(ultimateDefense, (player, skill, target) -> player.getHpPercentage() < 40));
    }
}