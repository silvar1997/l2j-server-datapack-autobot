package com.l2jserver.datapack.autobots.behaviors.classes.pre;


import com.l2jserver.datapack.autobots.Autobot;
import com.l2jserver.datapack.autobots.behaviors.CombatBehavior;
import com.l2jserver.datapack.autobots.behaviors.attributes.MiscItem;
import com.l2jserver.datapack.autobots.behaviors.attributes.RequiresMiscItem;
import com.l2jserver.datapack.autobots.behaviors.attributes.SecretManaRegen;
import com.l2jserver.datapack.autobots.behaviors.preferences.CombatPreferences;
import com.l2jserver.datapack.autobots.behaviors.preferences.SkillPreferences;
import com.l2jserver.datapack.autobots.skills.BotSkill;
import com.l2jserver.datapack.autobots.utils.AutobotHelpers;
import com.l2jserver.gameserver.enums.ShotType;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;

import java.util.List;

import static com.l2jserver.datapack.autobots.utils.AutobotSkills.rapidShot;

public class RogueBehavior extends CombatBehavior implements RequiresMiscItem, SecretManaRegen {


    public RogueBehavior(Autobot player, CombatPreferences combatPreferences) {
        super(player, combatPreferences);
        this.skillPreferences = new SkillPreferences(false);
    }

    @Override
    public ShotType getShotType() {
        return ShotType.SOULSHOTS;
    }

    @Override
    protected List<BotSkill> getOffensiveSkills() {
        return List.of(
                new BotSkill(16),
                new BotSkill(96, (player, skill, target) -> target.getEffectList().isAffectedBySkill(96)),
                new BotSkill(101, (player, skill, target) -> target instanceof L2PcInstance && ((L2PcInstance) target).getPvpFlag() > 0 && !target.isStunned()));
    }

    @Override
    protected List<BotSkill> getSelfSupportSkills() {
        return List.of(
                new BotSkill(rapidShot, (player, skill, target) -> !player.getEffectList().isAffectedBySkill(rapidShot)),
                new BotSkill(111, (player, skill, target) -> player.getHpPercentage() < 40));
    }

    @Override
    public List<MiscItem> getMiscItems() {
        return List.of(new MiscItem(AutobotHelpers::getArrowId));
    }
}