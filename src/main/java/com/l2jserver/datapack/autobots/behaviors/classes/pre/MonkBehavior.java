package com.l2jserver.datapack.autobots.behaviors.classes.pre;


import com.l2jserver.datapack.autobots.Autobot;
import com.l2jserver.datapack.autobots.behaviors.CombatBehavior;
import com.l2jserver.datapack.autobots.behaviors.attributes.SecretManaRegen;
import com.l2jserver.datapack.autobots.behaviors.preferences.CombatPreferences;
import com.l2jserver.datapack.autobots.behaviors.preferences.SkillPreferences;
import com.l2jserver.datapack.autobots.skills.BotSkill;
import com.l2jserver.gameserver.enums.ShotType;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.skills.Skill;

import java.util.List;

public class MonkBehavior extends CombatBehavior implements SecretManaRegen {

    private final int maxCharges;

    public MonkBehavior(Autobot player, CombatPreferences combatPreferences) {
        super(player, combatPreferences);
        this.skillPreferences = new SkillPreferences(false);
        final Skill sonicMastery = player.getSkills().get(992);
        final Skill focusMastery = player.getSkills().get(993);
        maxCharges = (sonicMastery != null) ? sonicMastery.getLevel() : (focusMastery != null) ? focusMastery.getLevel() : 0;
    }

    @Override
    public ShotType getShotType() {
        return ShotType.SOULSHOTS;

    }

    @Override
    protected List<BotSkill> getOffensiveSkills() {


        return List.of(
                new BotSkill(120, (player, skill, target) -> target instanceof L2PcInstance && !target.isStunned()),
                new BotSkill(54, (player, skill, target) -> player.getCharges() < maxCharges),
                new BotSkill(284, (player, skill, target) -> player.getCharges() < maxCharges));
    }

    @Override
    protected List<BotSkill> getSelfSupportSkills() {
        return List.of(
                new BotSkill(50, (player, skill, target) -> player.getCharges() < maxCharges));
    }
}