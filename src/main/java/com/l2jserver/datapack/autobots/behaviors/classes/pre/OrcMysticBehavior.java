package com.l2jserver.datapack.autobots.behaviors.classes.pre;


import com.l2jserver.datapack.autobots.Autobot;
import com.l2jserver.datapack.autobots.behaviors.CombatBehavior;
import com.l2jserver.datapack.autobots.behaviors.attributes.SecretManaRegen;
import com.l2jserver.datapack.autobots.behaviors.preferences.CombatPreferences;
import com.l2jserver.datapack.autobots.behaviors.preferences.SkillPreferences;
import com.l2jserver.datapack.autobots.skills.BotSkill;
import com.l2jserver.gameserver.enums.ShotType;

import java.util.List;

public class OrcMysticBehavior extends CombatBehavior implements SecretManaRegen {
    public OrcMysticBehavior(Autobot player, CombatPreferences combatPreferences) {
        super(player, combatPreferences);
        this.skillPreferences = new SkillPreferences(player.getLevel() > 7);
    }

    @Override
    public ShotType getShotType() {

        return ShotType.BLESSED_SPIRITSHOTS;
    }

    @Override
    protected List<BotSkill> getOffensiveSkills() {
        return List.of(
                new BotSkill(1090));
    }

    @Override
    public void onLevelUp(int oldLevel, int newLevel) {
        if (player.getLevel() >= 7) {
            this.skillPreferences.isSkillsOnly = true;
        }
    }
}