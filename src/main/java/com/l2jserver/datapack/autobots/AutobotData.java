package com.l2jserver.datapack.autobots;

import com.l2jserver.datapack.autobots.config.AutobotBuffs;
import com.l2jserver.datapack.autobots.config.AutobotEquipment;
import com.l2jserver.datapack.autobots.config.AutobotSettings;
import com.l2jserver.datapack.autobots.config.AutobotSymbol;
import com.l2jserver.datapack.autobots.models.AutobotLocation;
import com.l2jserver.gameserver.config.Configuration;
import com.l2jserver.gameserver.model.base.ClassId;
import com.l2jserver.gameserver.model.holders.SkillHolder;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.l2jserver.datapack.autobots.utils.Util.mapper;

public class AutobotData {

    private List<AutobotEquipment> equipment;
    private Map<ClassId, List<SkillHolder>> buffs;
    private List<AutobotSymbol> symbols;
    private List<AutobotLocation> teleportLocations;
    private AutobotSettings settings;


    private AutobotData() {
        try {
            settings = mapper.readValue(new File(Configuration.server().getDatapackRoot() + "/data/autobots/config.xml"), AutobotSettings.class);
            equipment = mapper.readValue(new File(Configuration.server().getDatapackRoot() + "/data/autobots/equipment.xml"), mapper.getTypeFactory().constructCollectionType(List.class, AutobotEquipment.class));
            List<AutobotBuffs> autobotBuffs = mapper.readValue(new File(Configuration.server().getDatapackRoot() + "/data/autobots/buffs.xml"), mapper.getTypeFactory().constructCollectionType(List.class, AutobotBuffs.class));
            buffs = autobotBuffs.stream().collect(Collectors.toMap(AutobotBuffs::getClassId, AutobotBuffs::getBuffsContent));
            teleportLocations = mapper.readValue(new File(Configuration.server().getDatapackRoot() + "/data/autobots/teleports.xml"), mapper.getTypeFactory().constructCollectionType(List.class, AutobotLocation.class));
            symbols = mapper.readValue(new File(Configuration.server().getDatapackRoot() + "/data/autobots/symbols.xml"), mapper.getTypeFactory().constructCollectionType(List.class, AutobotSymbol.class));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public List<AutobotEquipment> getEquipment() {
        return equipment;
    }

    public Map<ClassId, List<SkillHolder>> getBuffs() {
        return buffs;
    }

    public List<AutobotSymbol> getSymbols() {
        return symbols;
    }

    public List<AutobotLocation> getTeleportLocations() {
        return teleportLocations;
    }

    public AutobotSettings getSettings() {
        return settings;
    }

    public static AutobotData getInstance() {
        return SingletonHolder.INSTANCE;
    }

    private static class SingletonHolder {
        static final AutobotData INSTANCE = new AutobotData();
    }


}
